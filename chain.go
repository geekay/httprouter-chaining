package server

import (
	"github.com/julienschmidt/httprouter"
)

// Constructor for a piece of middleware.
type Constructor func(httprouter.Handle) httprouter.Handle

// Chain acts as a list of httprouter.Handle constructors.
// Chain is effectively immutable:
// once created, it will always hold the same set of constructors in the same order.
type Chain struct {
	constructors []Constructor
}

// NewChain creates a new chain
func NewChain(constructors ...Constructor) Chain {
	return Chain{append(([]Constructor)(nil), constructors...)}
}

// Then chains the middleware and returns the final httprouter.Handle.
func (c Chain) Then(h httprouter.Handle) httprouter.Handle {
	for i := range c.constructors {
		h = c.constructors[len(c.constructors)-1-i](h)
	}

	return h
}

// Append extends a chain, adding the specified constructors
func (c Chain) Append(constructors ...Constructor) Chain {
	newCons := make([]Constructor, 0, len(c.constructors)+len(constructors))
	newCons = append(newCons, c.constructors...)
	newCons = append(newCons, constructors...)

	return Chain{newCons}
}

// Extend extends a chain by adding the specified chain
func (c Chain) Extend(chain Chain) Chain {
	return c.Append(chain.constructors...)
}
